#!/bin/bash
# VARIABLES
# each variable is created with a """random""" string based on the date
ROOT_MYSQL=$(date +%s | sha256sum | base64 | head -c 32 ; echo)
sleep 1 # waits a second so the passwords are not the same
GATEWAY_MYQSL=$(date +%s | sha256sum | base64 | head -c 32 ; echo)
sleep 1
REPLUSER_MYSQL=$(date +%s | sha256sum | base64 | head -c 32 ; echo)
sleep 1
POSTGRE=$(date +%s | sha256sum | base64 | head -c 32 ; echo)
# creates the file where the passwords are stored
touch /tmp/keys.yml
# writes in json all the passwords
echo "{ 
		\"ROOT_MYSQL\": "$ROOT_MYSQL",
		\"GATEWAY_MYQSL\": "$GATEWAY_MYQSL",
		\"REPLUSER_MYSQL\": "$REPLUSER_MYSQL",
		\"POSTGRE\": "$POSTGRE"
	}" > /tmp/keys.json